spec:
  inputs:
    stage:
      default: generation
      description: 'Defines the generate stage'
    private_token:
      default: ""
      description: 'Access token to access the project reports if private'
    generator_images_base:
      default: 'registry.gitlab.com/components/sbom/generator'
    generator_images_version:
      default: ':latest'
    system_name:
      default: $CI_PROJECT_NAME
      description: "Name of the software system described by the SBOM"
    project_path:
      description: "Project path to extract SBOM from. Example: 'gitlab-org/gitlab'"
    ref_name:
      default: ""
      description: "Git reference to extract SBOM from. Defaults to latest tag, retrieved automatically via the GitLab API"
    dependency_scanning_max_depth:
      default: 2
      type: number
      description: "Defines how many directory levels deep that the analyzer should search for supported files to scan"
    fetch_licenses:
      default: true
      description: "Use external services to download licenses data"
      type: boolean
    generator_debug:
      default: ""
      description: "Enable verbose output in shell scripts"
---

# Export the SBOM of the inputs.project for the inputs.ref_name.
# If the ref name is empty, the latest (semver) tag of the project is used.
.generator:
  stage: $[[ inputs.stage ]]
  image:
    name: $[[ inputs.generator_images_base | expand_vars ]]$[[ inputs.generator_images_version ]]
    entrypoint: [""]
  script:
    - /scripts/sbom.sh export
  variables:
    SYSTEM: $[[ inputs.system_name | expand_vars ]]
    DS_MAX_DEPTH: $[[ inputs.dependency_scanning_max_depth ]]
    PROJECT_PATH: $[[ inputs.project_path ]]
    REF_NAME: $[[ inputs.ref_name ]]
    GENERATOR_DEBUG: $[[ inputs.generator_debug ]]
    SBOM_GENERATOR_PRIVATE_TOKEN: $[[ inputs.private_token ]]
    FETCH_LICENSES: $[[ inputs.fetch_licenses ]]
  artifacts:
    paths:
      - sbom/
  cache:
    - key: licenses-cache
      paths:
        - cache/packages
    - key: "artifacts-$CI_JOB_NAME"
      paths:
        - cache/artifacts

# There should be only one job fetching and exporting the REF_NAME
# Other jobs extending .generator must not save the REF_NAME in `sbom.env` because we use the one from the main project in the `wrap` stage.
generate sbom:
  extends: .generator
  script:
    - source /scripts/common.sh
    - if [ -z "${REF_NAME}" ]; then REF_NAME=$(get_latest_version_from "${PROJECT_PATH}"); fi
    - echo "REF_NAME=$REF_NAME" > sbom.env
    - /scripts/sbom.sh export
  artifacts:
    reports:
      dotenv: sbom.env
