# 4. Store SBOMs in the Package Registry

Date: 2024-02-27

## Status

Accepted

## Context

Generated SBOMs must be stored somewhere to be exposed to users and customers. While CI/CD artifacts
provide a way to do so, they present different limitations, including:

* Poor presentation: There is no easy way to present all these artifacts in a way that users can
find the right pipeline, job, artifact, and therefore, SBOM.
* CI/CD artifacts expire after some time.
* If a SBOM needs to be regenerated, it's not obvious that a new version is available, and users
might just consider the older version.

## Decision

The [Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/index.html) is
leveraged to store generated SBOMs as generic packages. This feature provide many advantages:

* Packages are presented in a [dedicated
page](https://docs.gitlab.com/ee/user/packages/package_registry/index.html#view-packages) exposing
all he "packages" (understand: SBOMs).
* Each SBOM (generic package) can have multiple versions. The version is specified when [creating the
  package](https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file).
* Versions can be overridden. If we generate a new version and an existing SBOM, the [new version is
served and old one
kept](https://docs.gitlab.com/ee/user/packages/generic_packages/#publishing-a-package-with-the-same-name-or-version).
* The API is trivial to use, and doesn't require a specific token. A CI/CD Job Token can be used
directly to store the SBOM.
* Packages are securely stored in [Object
Storage](https://docs.gitlab.com/ee/administration/object_storage.html) so we don't have to manage
that part here.

## Consequences

What becomes easier or more difficult to do and any risks introduced by the change that will need to
be mitigated. SBOMs are now available via the Package Registry of the project running a Generator.
We need to point users to this location, and they can download the right version from the UI.

It's also possible to download the SBOM programmatically via the REST or the GraphQL API. For
example, to get a specific version, users can [list
packages](https://docs.gitlab.com/ee/api/packages.html#list-packages) having that version with the
`package_version` parameter.
