# 1. Use one generator per feature

Date: 2023-10-26

## Status

Deprecated

## Deprecation notice

This project has been converted to a reusable CI/CD Component since.

## Context

The initial version of this generator was a single Docker image to create the gitlab-core SBOM.
With the addition of other features, like gitlab-runner, this single image is too complex to cover
all needs.

Another problem with the future addition of features is the lack of flexibility of the current
implementation. All-in-one images would create all SBOMs while just some of them are needed
sometimes.

Last of but not least, these features have different life cycle: not all features projects release a
new version when gitlab-org/gitlab is updated.

## Decision

Each feature is managed in its own subfolder and has its own Dockerfile (hence Docker image).
Images are updated only if at least one of their files is updated.

## Consequences

Each image will have one "reference entrypoint" (not to be confused with a Docker entrypoint). This
means that there's one project specifically in each feature that holds the main git reference (commit
SHA, tag, ...). From this point, we need to do our best to fetch the corresponding versions of
dependencies or related projects, and not default to the last tag or the HEAD of the default branch
if possible.
