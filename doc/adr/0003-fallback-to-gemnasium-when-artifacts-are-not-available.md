# 3. Fallback to Gemnasium when artifacts are not available

Date: 2023-11-04

## Status

Accepted

## Context

CI/CD artifacts expire after some time, or simply don’t exist because Dependency Scanning didn't
run or fail.

## Decision

When artifacts aren't available, run Gemnasium to regenerate the SBOMs.

## Consequences

If Dependency Scanning is configured in a special way (to ignore directories for example), this
configuration isn't used when regenerating the SBOMs.
