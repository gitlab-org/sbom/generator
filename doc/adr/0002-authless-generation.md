# 2. Authless generation

Date: 2023-10-26

## Status

Accepted

## Context

It should be possible to generate SBOMs without the need for authentication. All projects are
public, therefore the information is as well. We don't cover private projects SBOMs at the moment,
and that will be another ADR if needed.

## Decision

The GitLab ["Export SBOM" tutorial](https://docs.gitlab.com/ee/tutorials/export_sbom.html) isn't
used in this project, for the following reasons:
- We need to get the SBOM for specific versions. While this feature allows to get the SBOM generated
  by a specified pipeline ID, it can add some extra steps to get this ID.
- Not all pipelines generate a SBOM at GitLab (this will be covered by the next ADR to be created)
- This feature requires a API token with the `api` scope (read-write on everything). Considering the
  number of projects we need to touch with this generator, this goes against our least privilege
principle.

## Consequences

SBOMs are fetched via the [jobs artifacts](https://docs.gitlab.com/ee/api/job_artifacts.html) API
endpoint. Unlike the Export SBOM feature, this endpoint doesn't require authentication, but provides
the same result.
