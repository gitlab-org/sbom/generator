#!/bin/bash

# shellcheck source=scripts/common.sh
source "$(dirname "$0")/../../scripts/common.sh"

function cleanup_gitlab_gitlab_org() {
  echo -en "${CYAN}Cleanup: ${NOCOLOR}"
  rm -rf ./sbom/gitlab-core/gitlab-org/gitlab/ee/spec
  # If ee is still empty, remove it altogether
  rmdir ./sbom/gitlab-core/gitlab-org/gitlab/ee || true
  rm -rf ./sbom/gitlab-core/gitlab-org/gitlab/{qa,storybook}
  echo -e "${GREEN}OK${NOCOLOR}"
}

function cleanup_gitlab_gitaly() {
  echo -en "${CYAN}Cleanup: ${NOCOLOR}"
  rm -rf ./sbom/gitlab-core/gitlab-org/gitaly/_support/
  echo -e "${GREEN}OK${NOCOLOR}"
}

function convert_manifest_to_cdx() {
  local MANIFEST=$1
  local CDX_ENVELOP
  CDX_ENVELOP="$(
    cat <<-EOF
			    {
			      "bomFormat": "CycloneDX",
			      "specVersion": "1.4",
			      "serialNumber": "urn:uuid:",
			      "version": 1,
			      "metadata": {
			        "timestamp": "",
			        "tools": [
			          {
			            "vendor": "GitLab",
			            "name": "SBOM Generator",
			            "externalReferences": [
			              {
			                "url": "https://gitlab.com/gitlab-org/sbom/generator",
			                "type": "vcs"
			              }
			            ]
			          }
			        ],
			        "authors": [
			          {
			            "name": "GitLab",
			            "email": "support@gitlab.com"
			          }
			        ]
			      }
			    }
		EOF
  )"

  {
    jq \
      --arg uuid "$(uuidgen)" \
      --arg timestamp "$(date -u +"%Y-%m-%dT%H:%M:%SZ")" \
      '.serialNumber |= . + $uuid |
      .metadata.timestamp = $timestamp' \
      <<<"$CDX_ENVELOP"
    jq '{components: .software
    | to_entries
    | map(
        {
          name: .key,
          version: .value.display_version,
          type: "library",
          purl: (
            "pkg/generic/" + .key 
            + (.value.display_version | if . != null then "@" + . else "" end)
            + (.value.locked_source | if .git != null or .url != null then "?download_url=" + (.git // .url) else "" end)
            + (.value.locked_source.sha256 | if . != null then "&checksum=sha256:" + . else "" end)
          ),
          licenses: [
            {license: {name: .value.license}}
          ]
        }
      )
    } | del(.. | nulls) ' "$MANIFEST"
  } | jq -s add >"sbom/$SYSTEM/gitlab-org/omnibus-gitlab/gl-sbom-manifest.cdx.json"
  cleanup_omnibus_manifest
}

function download_omnibus_manifest() {
  local REF_NAME
  REF_NAME=$1
  echo -en "${CYAN}Download Omnibus manifest for ref $REF_NAME: ${NOCOLOR}"
  mkdir -p "sbom/$SYSTEM/gitlab-org/omnibus-gitlab"
  curl --silent --show-error --header "PRIVATE-TOKEN: $SBOM_GENERATOR_PRIVATE_TOKEN" \
    --location \
    --remote-name \
    --fail \
    --output-dir "sbom/$SYSTEM/gitlab-org/omnibus-gitlab/" \
    "$(omnibus_manifest_url "$REF_NAME")"
  echo -e "${GREEN}OK${NOCOLOR}"
}

function omnibus_manifest_path() {
  local REF_NAME
  REF_NAME=$1
  echo "sbom/$SYSTEM/gitlab-org/omnibus-gitlab/${REF_NAME:1}.0-ee.version-manifest.json"
}

function omnibus_manifest_url() {
  local REF_NAME
  REF_NAME=$1
  echo "https://gitlab-org.gitlab.io/omnibus-gitlab/gitlab-manifests/gitlab-ee/$(echo "${REF_NAME:1}" | cut -d "." -f 1,2)/${REF_NAME:1}.0-ee.version-manifest.json"
}

function cleanup_omnibus_manifest() {
  find sbom -name '*-ee.0-ee.version-manifest.json' -exec rm {} \;
}
