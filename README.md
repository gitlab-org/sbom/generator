# GitLab SBOM Generator

This project is used to extract SBOMs from GitLab projects.

These components have been designed to ease the gathering of SBOMs from multiple projects in a
single location.

SBOMs are fetched from pipelines where GitLab's [Dependency
Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) has run. If the
artifacts are not available, Gemnasium (the main Dependency Scanning analyzer) will be launched
locally to reprocess the SBOMs of the project.

## Disclaimer

This project is in [experimental
stage](https://docs.gitlab.com/ee/policy/experiment-beta-support.html), and should not be considered
stable at this point. The interface and code are subject to breaking changes.

## Usage

This project provides 4 components to be added to an existing `.gitlab-ci.yml` file
by using the `include:` keyword. Example:

```yaml
stages:
  - generation
  - wrap
  - test
  - package

include:
  - component: gitlab.com/components/sbom/generator/generate@<VERSION>
    inputs:
      system_name: "gitlab-core"
      project_path: "gitlab-org/gitlab"
      dependency_scanning_max_depth: 3
  - component: gitlab.com/components/sbom/generator/wrap@<VERSION>
    inputs:
      system_name: "gitlab-core"
  - component: gitlab.com/components/sbom/generator/validate@<VERSION>
  - component: gitlab.com/components/sbom/generator/package@<VERSION>
```

Where `<VERSION>` is a released tag or `~latest` for the latest version available.

### Inputs

See https://gitlab.com/explore/catalog/components/sbom/generator

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components

## Examples

See how this component is used at GitLab: https://gitlab.com/gitlab-org/sbom/systems

## Architecture decisions

[Architecture Decision Records](https://adr.github.io/) are documented in the [doc/](./doc/) folder.
