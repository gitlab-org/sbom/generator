FROM registry.gitlab.com/security-products/gemnasium:5

RUN apk add --no-cache jq curl unzip bash git uuidgen
ADD ./scripts /scripts/

WORKDIR /export

ENTRYPOINT ["/scripts/sbom.sh"]
