#!/bin/bash

# strict mode - http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

DEBUG=${GENERATOR_DEBUG:-}
if [ -n "${DEBUG}" ]; then
  set -x
fi

declare GREEN='\e[0;32m'
declare CYAN='\e[1;36m'
declare RED='\e[1;31m'
declare NOCOLOR='\e[0m'
export GREEN CYAN RED NOCOLOR

# Token used to to authenticate against the GitLab API
# This can be left empty if all reports are in public pipelines
declare SBOM_GENERATOR_PRIVATE_TOKEN
SBOM_GENERATOR_PRIVATE_TOKEN=${SBOM_GENERATOR_PRIVATE_TOKEN:-""}

# Git reference of the project
# If empty, `get_latest_version_from` will be called to fetch the latest tag
declare REF_NAME
REF_NAME=${REF_NAME:-""}

# Disable the download of licences when generating SBOMs
declare FETCH_LICENSES
FETCH_LICENSES=${FETCH_LICENSES:-"true"}

function url_encode {
  printf %s "$1" | jq -sRr @uri
}

function download_artifacts() {
  local PROJECT
  PROJECT=$(url_encode "$1")
  local PROJECT_PATH
  PROJECT_PATH=$1
  local REF_NAME
  REF_NAME=$2
  echo -en "${CYAN}Download artifacts of project $1 for ref $REF_NAME: ${NOCOLOR}"

  QUERY="{
      \"query\": \"query {
        project(fullPath: \\\"$1\\\") {
          pipelines(ref: \\\"$REF_NAME\\\", scope:FINISHED, last: 1) {
            nodes {
              jobs(securityReportTypes:DEPENDENCY_SCANNING, statuses: SUCCESS) {
                nodes {
                  name
                  browseArtifactsPath
                }
              }
            }
          }
        }
      }\"
    }"

  local RESULTS
  RESULTS=$(curl "https://gitlab.com/api/graphql" \
    --silent --show-error --header "Authorization: Bearer $SBOM_GENERATOR_PRIVATE_TOKEN" \
    --header "Content-Type: application/json" --request POST \
    --data-raw "${QUERY//$'\n'/}" |
    jq --raw-output '.data.project.pipelines.nodes[].jobs.nodes[]
      | .browseArtifactsPath |= ("https://gitlab.com" + . | sub("browse$"; "download"))
      | .name, .browseArtifactsPath')
  if [ -z "$RESULTS" ]; then
    export_sbom_with_gemnasium "$PROJECT_PATH" "$REF_NAME"
    return
  fi
  echo "$RESULTS" |
    while
      read -r filename
      read -r artifact_download_url
    do
      curl --silent --show-error --header "PRIVATE-TOKEN: $SBOM_GENERATOR_PRIVATE_TOKEN" \
        --location \
        --fail \
        --output "sbom/$SYSTEM/$1/${filename}.zip" \
        "$artifact_download_url" || {
        export_sbom_with_gemnasium "$PROJECT_PATH" "$REF_NAME"
        return
      }
    done
  echo -e "${GREEN}OK${NOCOLOR}"
  decompress_artifacts "$PROJECT_PATH"
}

function download_archive() {
  local PROJECT
  PROJECT=$(url_encode "$1")
  local PROJECT_PATH
  PROJECT_PATH=$1
  local REF_NAME
  REF_NAME=$2
  mkdir -p "sbom/$SYSTEM/$PROJECT_PATH/tmp"
  echo -en "${CYAN}Download archive of project $PROJECT_PATH for ref $REF_NAME: ${NOCOLOR}"
  curl --silent --show-error --header "PRIVATE-TOKEN: $SBOM_GENERATOR_PRIVATE_TOKEN" \
    --location \
    --fail \
    --output "sbom/$SYSTEM/$PROJECT_PATH/tmp/archive.tar.gz" \
    "https://gitlab.com/api/v4/projects/$PROJECT/repository/archive?sha=$REF_NAME"
  echo -e "${GREEN}OK${NOCOLOR}"

  decompress_archive "$PROJECT_PATH"
}

function decompress_artifacts() {
  local PROJECT_PATH="$1"
  echo -en "${CYAN}Decompress artifacts: ${NOCOLOR}"
  find "sbom/$SYSTEM/$PROJECT_PATH" -name '*.zip' -exec sh -c 'i="$1"; unzip -o -d `dirname "$i"` "$i" 1>/dev/null' shell {} \;
  # cleanup zip files
  find "sbom/$SYSTEM/$PROJECT_PATH" -name '*.zip' -exec rm {} \;
  echo -e "${GREEN}OK${NOCOLOR}"
}

function cleanup_artifacts() {
  local PROJECT_PATH="$1"
  echo -en "${CYAN}Cleanup artifacts: ${NOCOLOR}"
  # Don't keep the properties related to reachability in SBOMs
  find . -name 'gl-sbom-*.cdx.json' -print0 |
    while IFS= read -r -d '' sbom_file; do
      jq 'del(.components[].properties | select((.[]?.name == "gitlab:dependency_scanning_component:reachability") or ((. |length) == 0)))' "$sbom_file" >"$sbom_file".tmp && mv "$sbom_file".tmp "$sbom_file"
    done
  echo -e "${GREEN}OK${NOCOLOR}"
}

function decompress_archive() {
  local PROJECT_PATH="$1"
  echo -en "${CYAN}Decompress archive: ${NOCOLOR}"
  cd "sbom/$SYSTEM/$PROJECT_PATH/tmp"
  tar xzf archive.tar.gz
  # cleanup zip files
  cd - >/dev/null
  rm "sbom/$SYSTEM/$PROJECT_PATH/tmp/archive.tar.gz"
  # cleanup fixtures and test files
  find "sbom/$SYSTEM/$PROJECT_PATH/tmp" -path '**/fixtures/**/gl-sbom*.cdx.json' -delete
  echo -e "${GREEN}OK${NOCOLOR}"
}

function run_gemnasium() {
  local PROJECT_PATH="$1"
  echo -e "${CYAN}Run Gemnasium on $1: ${NOCOLOR}"
  cd "sbom/$SYSTEM/$PROJECT_PATH/tmp/"*
  # In a CI/CD job, CI_PROJECT_DIR is set and will make gemnasium run on the project root instead of this dir
  # By Setting this variable, we force the analyzer to run where specified
  # See https://gitlab.com/gitlab-org/security-products/analyzers/command/-/blob/a47d1efd778cc916696f84d29f727e7670608c82/run.go#L34
  # and https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/blob/4d4f9505f2b791dcc9dd76e3a11f4a715095c1f5/cli/flags/flags.go#L21
  export ANALYZER_TARGET_DIR=$PWD
  /analyzer sbom
  find . -name 'gl-sbom-*.cdx.json' -print0 |
    while IFS= read -r -d '' sbom_file; do
      mkdir -p "../../$(dirname "$sbom_file")"
      mv "$sbom_file" "../../$(dirname "$sbom_file")/"
    done
  cd - >/dev/null
  rm -rf "sbom/$SYSTEM/$PROJECT_PATH/tmp"
  echo -e "${GREEN}OK${NOCOLOR}"
}

function export_sbom() {
  local SYSTEM=${1:-${SYSTEM:-}}
  local PROJECT_PATH=${2:-${PROJECT_PATH:-}}
  local REF_NAME=${3:-${REF_NAME:-}}

  if [ -z "$SYSTEM" ] || [ -z "$PROJECT_PATH" ]; then
    echo "Usage: $0 export <SYSTEM> <PROJECT> [REF_NAME]"
    exit 1
  fi

  if [ -z "${REF_NAME}" ]; then
    REF_NAME=$(get_latest_version_from "${PROJECT_PATH}")
  fi

  if ! mount | grep /export && [ -z "$CI" ]; then
    echo -e "${RED}Warning: /export is not mounted. Use 'docker run [...] -v \$PWD:/export [...]' to save exported sbom outside container.${NOCOLOR}"
  fi

  local sbom_path="sbom/$SYSTEM/$PROJECT_PATH"
  mkdir -p "$sbom_path"

  local artifacts_path="cache/artifacts/$SYSTEM/$PROJECT_PATH/$REF_NAME/"
  if [ -d "$artifacts_path" ] && [ "$(ls -A "$artifacts_path")" ]; then
    echo -e "${CYAN}Using cached artifacts from $artifacts_path${NOCOLOR}"
    cp -r "$artifacts_path"/* "$sbom_path"
  else
    download_artifacts "$PROJECT_PATH" "$REF_NAME"
  fi
  cleanup_artifacts "$PROJECT_PATH"

  if [ "$FETCH_LICENSES" != "true" ]; then
    echo -e "${CYAN}Skipping add licenses${NOCOLOR}"
    return
  fi
  add_licenses "sbom/$SYSTEM/$PROJECT_PATH"
  if [ "${DISABLE_CSV_GENERATION:-false}" != "true" ]; then
    generate_csv "sbom/$SYSTEM/$PROJECT_PATH"
  fi
}

function export_sbom_with_gemnasium() {
  local PROJECT_PATH="$1"
  local REF_NAME="$2"
  local SBOM_PATH="sbom/$SYSTEM/$PROJECT_PATH"

  echo -e "${CYAN}Artifacts not available, regenerating SBOMs with Gemnasium.${NOCOLOR}"
  mkdir -p "$SBOM_PATH"
  download_archive "$PROJECT_PATH" "$REF_NAME"
  run_gemnasium "$PROJECT_PATH"
  # Artifacts can't be cached in the sbom/ folder directly because we don't know the REF_NAME in advance
  # Restoring the cache would also bring all versions of the cache, so we need another folder to pick-up only the version for REF_NAME (that we know at this point)
  cache_artifacts "$SBOM_PATH" "cache/artifacts/$SYSTEM/$PROJECT_PATH/$REF_NAME"
}

function cache_artifacts() {
  local SBOM_PATH="$1"
  local CACHE_PATH="$2"

  echo -en "${CYAN}Caching Gemnasium artifacts: ${NOCOLOR}"
  mkdir -p "$CACHE_PATH"
  if [ -z "$(ls -A "$SBOM_PATH")" ]; then
    echo "$SBOM_PATH is empty"
    echo -e "${GREEN}SKIPPED${GREEN}"
    return
  fi
  cp -r "$SBOM_PATH"/* "$CACHE_PATH"
  echo -e "${GREEN}OK${NOCOLOR}"
}

function generate_csv() {
  local SBOM_PATH="$1"

  echo -en "${CYAN}Converting SBOM files to CSV: ${NOCOLOR}"

  find "$SBOM_PATH" -name 'gl-sbom-*.cdx.json' -print0 | while IFS= read -r -d '' sbom_file; do
    if ! jq -n 'inputs.components | unique | (.[].licenses | select(. != null)) |= ([.[].license.name] | join(" + "))' "$sbom_file" |
      jq -s 'flatten | (map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @tsv' >"${sbom_file%.json}.csv"; then
      echo -e "${RED}Failed to convert $sbom_file${NOCOLOR}"
      exit 1
    fi
  done

  echo -e "${CYAN}OK${NOCOLOR}"
}

# get_latest_version_from fetches all tags from a repository ($PROJECT), sort them, and extract the latest
function get_latest_version_from() {
  local PROJECT
  PROJECT=$(url_encode "$1")
  curl --silent --show-error --header "PRIVATE-TOKEN: $SBOM_GENERATOR_PRIVATE_TOKEN" \
    --location \
    --fail \
    "https://gitlab.com/api/v4/projects/$PROJECT/repository/tags?per_page=50" |
    jq -r 'map(select(.name | test("(-alpha|-beta|-rc)") | not) | .name) | sort_by( . | capture("v?(?<version>\\d+.\\d+.\\d+)").version | split(".") | map(tonumber)) | last '
}

function create_sbom_wrapper() {
  local SYSTEM=${1:-${SYSTEM:-}}
  local REF_NAME=${2:-${REF_NAME:-}}

  echo -e "${CYAN}Generating SBOM wrapper for $SYSTEM ($REF_NAME)${NOCOLOR}"

  WRAP=$(jq -n \
    --arg uuid "$(uuidgen)" \
    --arg timestamp "$(date -u +"%Y-%m-%dT%H:%M:%SZ")" \
    --arg system "$SYSTEM" \
    --arg ref_name "$REF_NAME" \
    '
{
  "$schema": "http://cyclonedx.org/schema/bom-1.4.schema.json",
  "bomFormat": "CycloneDX",
  "specVersion": "1.4",
  "serialNumber": ("urn:uuid:" + $uuid),
  "version": 1,
  "metadata": {
    "timestamp": $timestamp,
    "tools": [
      {
        "vendor": "GitLab",
        "name": "Gemnasium"
      }
    ],
    "component": {
      "type": "application",
      "name": $system,
      "version": $ref_name
    },
  },
  "components": []
}')

  # Needed to get projects in "sbom/$SYSTEM"/*/*
  shopt -s globstar

  for project in "sbom/$SYSTEM"/*/*; do
    if [ -z "$(ls -A "$project")" ]; then
      echo "  Skip empty project $project"
      continue
    fi
    echo "  Adding $(find "$project" -name '*.cdx.json' | wc -l) SBOM(s) from $project"
    EXTERNAL_REFERENCES=$(
      find "$project" -name '*.cdx.json' -exec sha512sum {} \; |
        while
          read -r checksum filename
        do
          jq -r --arg checksum "$checksum" --arg filename "$filename" '
    {
      "type": "bom",
      "url": ("urn:cdx:" + .serialNumber + "/" + (.version | tostring)),
      "comment": $filename,
      "hashes": [
        {
          "alg": "SHA-512",
          "content": $checksum
        }
      ]
    }
  ' "$filename"
        done |
        jq -s '.'
    )

    TMP_WRAP=$(jq --arg project "$project" --arg group "$(dirname "$project")" --argjson externalRefs "$EXTERNAL_REFERENCES" \
      '.components += [
      {
        "type": "application",
        "group": $group,
        "name": $project,
        "version": "",
        "externalReferences": $externalRefs
      }]
     ' <<<"$WRAP")
    WRAP=$TMP_WRAP
  done

  shopt -u globstar

  echo "$WRAP" >"sbom/$SYSTEM/sbom.json"
}

function get_version_from_sbom() {
  local PACKAGE
  PACKAGE="$1"
  local SBOM_PATH
  SBOM_PATH="$2"
  local VERSION
  VERSION=$(jq -r --arg package "$PACKAGE" '.components[] | select(.name == $package) | .version' "$SBOM_PATH")
  # Go stores references in this format when a git commit sha is used (HEAD) instead of a git tag.
  if [[ "$VERSION" =~ ^v0\.0\.0-[[:digit:]]{14}-[[:alnum:]]{12}$ ]]; then
    VERSION=$(echo "$VERSION" | cut -d "-" -f 3)
  fi
  echo "$VERSION"
}

function add_licenses() {
  local PROJECT_LOCAL_PATH
  PROJECT_LOCAL_PATH=$1
  echo -e "${CYAN}Adding licenses to project $PROJECT_LOCAL_PATH: ${NOCOLOR}"

  find "$PROJECT_LOCAL_PATH" -name '*.json' -print0 |
    while IFS= read -r -d '' sbom_file; do
      echo -en "Adding $(jq -r '.components | length' "$sbom_file") licenses to $sbom_file: "
      for purl in $(jq -r '.components[].purl' "$sbom_file"); do
        encoded_purl=$(url_encode "$purl")
        if [[ "$purl" =~ ^pkg:([^/]+)/.+@.+ ]]; then
          package_type=${BASH_REMATCH[1]}
        else
          echo "Can't get package type from $purl"
          continue
        fi
        package_path="cache/packages/${package_type}"
        mkdir -p "$package_path"
        package_file="${package_path}/${encoded_purl}.json"
        # Lookup cache file with exact name, then try to match with version stripped, then fallback to download it
        if [ ! -f "$package_file" ]; then
          # see https://gitlab.com/gitlab-org/sbom/licenses#version-matching
          if [ -f "${package_file%'%40'*}.json" ]; then
            cp "${package_file%'%40'*}.json" "$package_file"
          else
            # Download package info from deps.dev if not present in cache
            case $(basename "$sbom_file") in
              "gl-sbom-gem-bundler.cdx.json")
                # extract package name and version from purl
                if [[ "$purl" =~ ^pkg:gem/(.+)@(.+) ]]; then
                  name=${BASH_REMATCH[1]}
                  version=${BASH_REMATCH[2]}
                  # Rubygems API doc: https://guides.rubygems.org/rubygems-org-api-v2/
                  if ! curl --silent --show-error --fail --output "$package_file" "https://rubygems.org/api/v2/rubygems/${name}/versions/${version}.json"; then
                    echo "Failed to fetch licenses for $purl"
                    continue
                  fi
                else
                  echo "Failed to get package name and version from $purl"
                  continue
                fi
                ;;

              *)
                # Deps.dev API doc: https://docs.deps.dev/api/v3alpha/#purl-parameters
                if ! curl --silent --show-error --fail --output "$package_file" "https://api.deps.dev/v3alpha/purl/$encoded_purl"; then
                  echo "Failed to fetch licenses for $purl"
                  continue
                fi
                ;;
            esac
          fi
        fi
        #  Deps.dev reports licenses under .version.licenses, but rubygems under .licences directly
        licenses=$(jq -c '.version.licenses? // .licenses' "$package_file")
        if [ -n "$licenses" ] && [ "$licenses" != "[]" ]; then
          jq --argjson licenses "$licenses" --arg purl "$purl" '(.components[] | select(.purl == $purl)).licenses |= ($licenses | map({license: {name: .}}))' "$sbom_file" >"$sbom_file.tmp" && mv "$sbom_file.tmp" "$sbom_file" || echo "Failed to add $licenses for package $purl"
        fi
      done
      echo -e "${GREEN}OK${NOCOLOR}"
    done
}
