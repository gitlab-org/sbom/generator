#!/bin/bash

# shellcheck source=scripts/common.sh
source "$(dirname "$0")/common.sh"

function usage() {
  echo "Usage: $0 <command> [parameters]"
  echo "  Valid commands (params in caps can be environment vars):"
  echo ""
  echo "  export <SYSTEM> <PROJECT_PATH> [REF_NAME]: Export sbom with a project path"
  echo "  wrap [REF_NAME]: Once exported, create a wrapper file to reference all SBOM files exported"
  exit 1
}

case "${1:-}" in
  export)
    shift
    export_sbom "$@"
    ;;

  wrap)
    shift
    create_sbom_wrapper "$@"
    ;;

  *)
    usage
    ;;
esac

echo "Done"
